package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class ProdutoServiceTeste {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    private Produto produto;
    private List<Produto> produtos;

    @BeforeEach
    private void setUp() {
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Livro");
        produto.setDescricao("Livro de biologia");
        produto.setPreco(100.0);

        produtos = Arrays.asList(produto);
    }

    @Test
    public void testarCriarProduto() {
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Assertions.assertEquals(produto, produtoService.criarProduto(produto));
    }

    @Test
    public void testarConsultarProdutos() {

        Mockito.when(produtoRepository.findAll()).thenReturn(produtos);
        List<Produto> produtosGravados = produtoService.consultarProdutos();

        Assertions.assertArrayEquals(produtos.toArray(), produtosGravados.toArray());
    }

    @Test
    public void testarConsultarProdutoPorId () {
    }

    @Test
    public void testarAtualizarProduto () {

    }

    @Test
    public void testarDeletarProduto () {
        Mockito.when(produtoRepository.existsById(Mockito.any())).thenReturn(true);

        //Mockito.when(produtoRepository.deleteById(Mockito.anyInt())).thenReturn(produto);
    }

}
