package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeadRepository extends CrudRepository<Lead, Integer> {

    Lead findByCpf(String cpf);

    List<Lead> findByProdutosId(int id);

}
