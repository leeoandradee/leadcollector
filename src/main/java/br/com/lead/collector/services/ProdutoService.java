package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public Produto criarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public List<Produto> consultarProdutos() {
        return (List<Produto>) produtoRepository.findAll();
    }

    public Produto consultarProdutoPorId(int id) {
        Optional<Produto> produto = produtoRepository.findById(id);
        if (produto.isPresent()) {
            return produto.get();
        } else {
            throw new RuntimeException("O produto não foi encontrado");
        }
    }

    public Produto atualizarProduto(int id, Produto produto) {
        Produto produtoDb = consultarProdutoPorId(id);
        produto.setId(produtoDb.getId());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("O produto não foi encontrado");
        }
    }
}
