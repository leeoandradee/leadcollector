package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead salvarLead(CadastroDeLeadDTO lead) {

        Lead leadObj = lead.converterPAraLead();

        List<Integer> idDeProdutos = new ArrayList<>();
        for (IdProdutoDTO idProduto: lead.getProdutos()) {
            idDeProdutos.add(idProduto.getId());
        }

        Iterable<Produto> produtos = produtoRepository.findAllById(idDeProdutos);

        leadObj.setProdutos((List<Produto>) produtos);

        return leadRepository.save(leadObj);
    }

    public List<Lead> consultarLeads() {
        return (List<Lead>) leadRepository.findAll();
    }

    public Lead consultarLeadPorId(int id) {
        Optional<Lead> optional = leadRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }

    public Lead atualizarLead(int id, Lead lead) {
        Lead leadDb = consultarLeadPorId(id);
        lead.setId(leadDb.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead(int id) {
        if (leadRepository.existsById(id)) {
           leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }

    public Lead findByCpf(String cpf) {
        Lead lead = leadRepository.findByCpf(cpf);
        if (lead != null) {
            return lead;
        } else {
            throw new RuntimeException("CPF não encontrado");
        }
    }




}

