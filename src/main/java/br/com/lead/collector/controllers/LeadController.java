package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController()
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    LeadService leadService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Lead salvarLead(@RequestBody @Valid CadastroDeLeadDTO lead) {
        return leadService.salvarLead(lead);
    }

    @GetMapping()
    public List<Lead> consultarLeads() {
        return leadService.consultarLeads();
    }

    @GetMapping("/{id}")
    public Lead consultarLeadPorId(@PathVariable(name = "id") int id) {
        try {
            return leadService.consultarLeadPorId(id);
        } catch (RuntimeException e) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead consultarLeadPorId(@PathVariable(name = "id") int id, @RequestBody @Valid Lead lead) {
        try {
            return leadService.atualizarLead(id, lead);
        } catch (RuntimeException e) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLeadPorId(@PathVariable(name = "id") int id) {
        try {
         leadService.deletarLead(id);
        } catch (RuntimeException e) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/detalhes")
    public Lead detalhesLead(@RequestParam(name = "cpf") String cpf) {
        try {
            return leadService.findByCpf(cpf);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }



}
